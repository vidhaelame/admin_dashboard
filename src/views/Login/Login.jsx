import React from "react";
import LoginPopup from "../../components/LoginPopup/LoginPopup";
import { Box, Image } from "@chakra-ui/react";
import LoginPopup2 from "../../components/LoginPopup/LoginPopup2";




const Login = () => {
  return (
    <Box
      className="Login"
      position="relative"
      height="100vh"
      width="100%"
      display="flex"
      flexDirection="column"
    >
      <Box
        className="login-top"
        flex="1"
        backgroundColor="#f5f5f5"
        display="flex"
        justifyContent="center"
        alignItems="start"
        boxSizing="border-box"
      >
        <Box className="logo-img" paddingTop="60px">
          <Image
            width="220px"
            height="50p;"
            src="../src/assets/images/Kids.png"
            alt="icon kids"
          />
        </Box>
      </Box>
      <Box
        className="login-bottom"
        flex="1"
        background="linear-gradient(180deg, #2a4558 0%, #05253a 100%)"
        display="flex"
        justifyContent="center"
        alignItems="end"
      >
        <Box
          as="p"
          fontStyle="normal"
          fontWeight="400"
          fontSize="0.7rem"
          color="#FFFFFF"
          marginBottom="20px"
        >
          Privacy policy disclaimer.
        </Box>
      </Box>
      <LoginPopup />
      {/* <LoginPopup2 /> */}
    </Box>
  );
};

export default Login;
