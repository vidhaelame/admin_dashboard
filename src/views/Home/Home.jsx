import React, { useState } from "react";
import { Outlet } from "react-router-dom";
import ProfilMenu from "../../components/ProfilMenu/ProfilMenu";
import MenuLeft from "../../components/MenuLeft/MenuLeft";
import { Box} from "@chakra-ui/react";



const Home = () => {
  const [ open, setOpen ] = useState(false);

  return (
    <Box position="relative" display="flex" height="100vh;" className="Home">
      <MenuLeft setOpen={setOpen} open={open} />
       <Box
         className="shadow-box"
         display={open=== false?"none": "flex"}
         position={"absolute"}
         zIndex={1}
         top={0}
         left={0}
         width={"100%"}
         height={"100vh"}
         backgroundColor="#1710104f"
         onClick={()=>
          setOpen(false) 
          }
        />
      <Box
        flex={4.8}
        display="flex"
        flexDirection="column"
        overflowY="auto"
        className="home-content"
      >
        <ProfilMenu setOpen={setOpen} open={open} />
        <Outlet />
      </Box>
    </Box>
  );
};

export default Home;
