import React from "react";
import { Box, Button, Image } from "@chakra-ui/react";
import { Link } from "react-router-dom";




const CourseItem = ({ course }) => {
  return (
    <Box
      className="course-item"
      boxSizing="border-box"
      // width="280px"
      height="242px"
      background="#1d3a4c"
      border="1px solid #e9ebf8"
      border-radius="5px"
      display="flex"
      flexDirection="column"
      alignItems="start"
      justifyContent="start"
      paddingTop="40px"
      position="relative"
      gap="25px"
      paddingLeft="30px"
    >
      <Box className="part1">
        <Box className="design1">
          <Image
            height="30px"
            width="20px"
            src="../src/assets/images/book.svg"
            alt="icon book"
          />
        </Box>
        <Box
          className="name-course-item"
          fontWeight="700"
          fontSize={{ base: "1.5rem", sm: "1.5rem",  md: "2rem", lg: "2rem", xl:"2rem" }}
          color="#ffffff"
          lineHeight="1.2"
        >
          {course.title}
        </Box>
      </Box>
      <Box className="view-course-btn"> 
        <Button
          padding={{ base: "0 5px", sm: "0 5px", md: "0 5px", lg: "auto", xl:"auto" }}
          fontWeight={400}
          height="32px"
          display="flex"
          justifyContent="start"
          color="#05253A"
          fontSize="0.9rem"
         // width="150px"
          width="fit-content"
          leftIcon={
            <Image
            display={{ base: "none", sm: "none", md: "none", lg: "block", xl:"block" }}
            width="20px"
            height="21px"
            src="../src/assets/images/launch.svg"
            alt=""
          />
          }
          colorScheme="#E2DDBF;"
        >
          <Link to="">{course.btnName}</Link>
        </Button>
      </Box>
      <Box className="design2" position="absolute" right="0" bottom="0">
        <img src="../src/assets/images/design2.svg" alt="" />
      </Box>
    </Box>
  );
};

export default CourseItem;
