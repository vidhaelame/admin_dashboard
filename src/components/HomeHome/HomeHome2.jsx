import React from "react";
import coursesData from "../../utils/coursesData";
import CourseItem from "../CourseItem/CourseItem";
import { Box } from "@chakra-ui/react";


import GridLayout from 'react-grid-layout';
import 'react-grid-layout/css/styles.css';
import 'react-resizable/css/styles.css';

const HomeHome = () => {
    const layoutConfig = [];

    coursesData.forEach((element, ind) => {
        layoutConfig.push({ i: 'item'+ind, x: 0, y: 0, w: 2, h: 3 });
        return layoutConfig;
    });

    console.log(layoutConfig);

  return (
    <Box backgroundColor="#f5f5f5" className="HomeHome">
      <Box
        padding="10px 20px"
        fontWeight="400"
        fontSize="1.4rem"
        color="#000000"
        className="title-home"
      >
        Home
      </Box>
      <Box
        boxSizing="border-box"
        padding="27px 20px"
        margin="0 20px 10px 20px"
        background="#ffffff"
        border="1px solid #e9ebf8"
        borderRadius="8px"
        className="home-courses"
      >
        {/* <Box
          className="courses-items"
          display="flex;"
          gap="20px;"
          flexWrap="wrap;"
        >
          {coursesData.map((course) => {
            return <CourseItem key={course.title} course={course} />;
          })}
        </Box> */}

      <GridLayout style={{
          display:"flex;",
          gap:"20px;",
          flexWrap:"wrap;"}}
       className="example-layout" layout={layoutConfig} cols={12} rowHeight={30} width={1200}>
         {coursesData.map((course, ind) => {
            return <CourseItem key={"item"+ind} course={course} />;
          })}
          

       </GridLayout>
      </Box>
    </Box>
  );
};

export default HomeHome;
