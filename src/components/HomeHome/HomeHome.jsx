import React from "react";
import coursesData from "../../utils/coursesData";
import CourseItem from "../CourseItem/CourseItem";
import { Box } from "@chakra-ui/react";



const HomeHome = () => {
  return (
    <Box backgroundColor="#f5f5f5" className="HomeHome">
      <Box
        padding="10px 20px"
        fontWeight="400"
        fontSize="1.4rem"
        color="#000000"
        className="title-home"
      >
        Home
      </Box>
      <Box
        boxSizing="border-box"
        padding="27px 20px"
        margin="0 20px 10px 20px"
        background="#ffffff"
        border="1px solid #e9ebf8"
        borderRadius="8px"
        className="home-courses"
      >
        <Box
          className="courses-items"
          gap="20px;"
          display= "grid;"
          gridTemplateColumns={{ base: "1fr;", sm: "1fr 1fr;", md: "1fr 1fr;", lg: "1fr 1fr 1fr;", xl:"1fr 1fr 1fr;" }}
        >
          {coursesData.map((course, ind) => {
            return <CourseItem key={course.title} course={course}  />;
          })}
        </Box>
      </Box>
    </Box>
  );
};

export default HomeHome;
