import React from "react";
import { Avatar, Box, Image } from "@chakra-ui/react";
import { HamburgerIcon } from "@chakra-ui/icons";




const ProfilMenu = ({setOpen, open}) => {
  return (
    <Box
      backgroundColor="#ffffff;"
      display="flex"
      justifyContent={{ base: "space-between", sm: "space-between", md: "end", lg: "end", xl:"end" }}
      alignItems="center"
      padding="10px 0"
      className="profile-menu"
      cursor="pointer"
    >
      <HamburgerIcon 
        onClick={()=>
          setOpen(true) 
        }
        display={{ base: "flex", sm: "flex", md: "none", lg: "none", xl:"none" }}
        width="24px" 
        height="24px" 
        marginLeft="10px"/>
      <Box
        display="flex"
        gap="30px"
        alignItems="center"
        paddingRight={50}
        className="profile-menu-content"
      >
        <Box className="icon-notif">
          <Image
            boxSize="24px"
            width="100%"
            src="../src/assets/images/notif.svg"
            alt="icon notif"
          />
        </Box>
        <Box
          display="flex"
          gap="20px"
          alignItems="center"
          className="infos-user"
        >
          <Box
            fontWeight="400"
            fontSize="0.9rem"
            color="#0a0507;"
            className="profil-name"
          >
            Elame Vidhal
          </Box>
          <Box className="profil-img">
            <Avatar
              height={50}
              width={50}
              name="Elame Vidhal"
              src="../src/assets/images/terra.jpg"
            />
          </Box>
          <Box className="profil-chevr">
            <Image
              boxSize="24px"
              width="100%"
              src="../src/assets/images/morechevron.svg"
              alt="chevron"
            />
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default ProfilMenu;
