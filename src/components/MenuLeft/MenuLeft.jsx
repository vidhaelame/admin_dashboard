import React, { useState } from "react";
import { Link } from "react-router-dom";
import dataItemMenu from "../../utils/dataItemMenu";
import ItemMenuLeft from "../ItemMenuLeft/ItemMenuLeft";
import { Box, Button, Image } from "@chakra-ui/react";





const MenuLeft = ({open, setOpen}) => {
  console.log("open", open)
  return (
    <Box
      flex={1}
      display={{ base: open=== false?"none": "flex" , sm: open=== false?"none": "flex" , md: "flex", lg: "flex", xl:"flex" }}
      position={{ base: "absolute", sm: "absolute", md: "static", lg: "static", xl:"static" }}
      zIndex={2}
      top={0}
      left={0}
      width={{ base: "55%", sm: "55%", md: "auto", lg: "auto", xl:"auto" }}
      height={{ base: "100vh", sm: "100vh", md: "auto", lg: "auto", xl:"auto" }}
      backgroundColor="#05253a;"
      padding="0 20px"
      boxSizing="border-box"
      flexDirection="column"
      className="menu-left"
    >
      <Box
        padding="10px 0 40px 0"
        display="flex"
        justifyContent="center"
        className="title"
      >
        <Image
          boxSize="60px"
          width="100%"
          src="../src/assets/images/Group7.svg"
          alt="icon seven"
        />
      </Box>
      <Box className="menu-left-content">
        <Box className="dashboard-btn">
          <Link onClick={()=>
                setOpen(false) 
                } to="/home">
            <Button 
              fontSize="0.9rem"
              fontWeight={400}
              height="40px"
              mb={2}
              display="flex"
              justifyContent="start"
              width="100%"
              leftIcon={
                <img
                  width={20}
                  height={20}
                  src="../src/assets/images/dashboard.svg"
                  alt=""
                />
              }
              colorScheme="#4C847B;"
            >
              <Box as="p"
                display="block"
              >Dashboard</Box>
            </Button>
          </Link>
          <Box
            height="2px"
            backgroundColor="rgba(255, 255, 255, 0.2);"
            width="100%"
            marginBottom="10px"
            className="border-line"
          ></Box>
        </Box>
        <Box className="items-dashboard">
          {dataItemMenu.map((item, ind) => {
            return (
              <ItemMenuLeft
                key={item.name}
                name={item.name}
                link={item.link}
                ind={ind}
                navLink={item.navLink}
                dataLenght={dataItemMenu.length - 1}
                setOpen={setOpen}
              />
            );
          })}
        </Box>
        <Box className="logout-btn">
          <Link to="/login">
            <Button
              fontWeight={400}
              height="40px"
              mt={3}
              color="black"
              fontSize="0.9rem"
              width="100%"
              leftIcon={
                <img
                  width={20}
                  height={20}
                  src="../src/assets/images/logout.svg"
                  alt=""
                />
              }
              colorScheme="#E2DDBF;"
            >
              <Box as="p"
                display="block"
              >Logout</Box>
            </Button>
          </Link>
        </Box>
      </Box>
    </Box>
  );
};

export default MenuLeft;
