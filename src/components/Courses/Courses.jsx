import React , { useState, useEffect } from 'react';
import './Courses.css';
import { Box, Button } from '@chakra-ui/react';
import { Link } from 'react-router-dom';
import { Space, Table, Tag } from 'antd';
import { useQuery } from '@tanstack/react-query';
import axios from 'axios'





const columns = [
  {
    title: 'Course Id',
    dataIndex: 'courseId',
    key: 'courseId',
    render: (text) => <a>{text}</a>,
  },
  {
    title: 'Course Name',
    dataIndex: 'CourseName',
    key: 'CourseName',
  },
  {
    title: 'Enrollment Date',
    dataIndex: 'EnrollmentDate',
    key: 'EnrollmentDate',
    render: (_, object) => (
      <Box className="enrollment">
        <Box as="p">{object.EnrollmentDate}</Box>
        <Box as="p" className='enrollTime'
              fontStyle= "normal"
              fontWeight= "300"
              fontSize= "0.6rem" 
              color= "#5d5a5b"
        >{object.EnrollmentTime}</Box>
      </Box>
    ),
  },
  {
    title: 'Duration',
    dataIndex: 'duration',
    key: 'duration',
  },
  {
    title: 'Status',
    key: 'status',
    dataIndex: 'status',
    render: (_, { status }) => (
      <>
        {status.map((oneStatus) => {
          let color = "btnPending";
          if (oneStatus === 'completed') {
            color = 'btnComplete';
          }
          return (
             <Box className={color} key={oneStatus}
                   display= "flex"
                   padding= "5px"
                   width= {oneStatus === 'completed' ? "64px" : "66px"}
                   background= {oneStatus === 'completed' ? "#e6f6d1" :" #eeeced"}
                   borderRadius= "5px"
                   color= {oneStatus === 'completed' ? "#27ae60" : null}
             >
               {oneStatus}
             </Box>
          );
        })}
      </>
    ),
  },
  {
    title: 'Action',
    key: 'action',
    render: (_, { actions }) => (
      <>
        {actions.map((oneAction) => {
          let srcCurrent =""
          let srcYes = "../src/assets/images/yesGreen.svg";
          let srcNo =  "../src/assets/images/croixRed.svg";
          let srcMessg = "../src/assets/images/messg.svg";
          oneAction === 'yes' ? srcCurrent = srcYes : null
          oneAction === 'no' ? srcCurrent = srcNo : null
          oneAction === 'message' ? srcCurrent = srcMessg : null
          return (
            <Space size="small" key={oneAction}>
              &nbsp;<a><img src={srcCurrent} alt="" /></a>
            </Space>
          );
        })}
      </>
    ),
  },
];

const Courses = () => {
  const [bottom, setBottom] = useState('bottomCenter');

  const REQUEST = "/src/utils/dataTable.json";

  const { isPending, error, data, isFetching } = useQuery({
    queryKey: ['dataInfos'],
       queryFn: async () => {
          const { data } = await axios.get(
            REQUEST,
          )
          return data
        },
  })
  //if (isPending) return 'Loading...'
 // if (error) return 'An error has occurred: ' + error.message




  return (
    <Box className="Courses"
      backgroundColor= "#f5f5f5"
    >
     <Box className="title-course"
         padding= "10px 20px" 
         fontWeight= "700"
         fontSize= "1.4rem"
         color= "#000000"
     >Courses</Box>
       <Box className="current-link"
              display= "flex"
              flexDirection= "row"
              alignItems= "center"
              gap= "3px"
              width= "148px"
              height= "16px"
              marginLeft= "20px"
              marginBottom= "20px"
       >
        <Box className="current1"
              display= "flex"
              gap= "2px"
              alignItems= "center"
        >
          <Box className="current-img">
            <img src="../src/assets/images/dashboard_gray.svg" alt="" />
          </Box>
          <Box className="current-name"
              fontWeight= "400"
              fontSize= "0.8rem"
              color= "#bdbdbd"
          >Dashboard /</Box>
        </Box>
        <Box className="current2"
            fontWeight= "400"
            fontSize= "0.8rem"
            color= "#05253a"
        >Courses</Box>
       </Box>
      <Box className="home-courses"
         boxSizing= "border-box"
         padding= "27px 37px"
         margin= "0 20px"
         background= "#ffffff"
         border= "1px solid #e9ebf8"
         borderRadius= "8px"
         marginBottom= "70px"
      >
        <Box  className="table-top"
              display= "flex"
              flexDirection={{ base: "column", sm: "column", md: "row", lg: "row", xl:"row" }}
              gap={{ base: "15px", sm: "15px", md: 0, lg: 0, xl: 0 }}
              justifyContent= "space-between"
              marginBottom= "20px"
        >
          <Box className="table-title"
            fontWeight= "700"
            fontSize= "1.2rem"
            color= "#000000"
            lineHeight= "1"
          >List of Courses</Box>
          <Box className="btn-add">
            <Button fontSize='0.9rem' 
               fontWeight={400} 
               height='40px'
               mb={2}  
               display="flex" 
              justifyContent={{ base: 'center', md: 'center', lg: 'start' }}
              width={{ base: "180.75px", sm: "180.75px", md: "100%"}}
              leftIcon={<img src="../src/assets/images/add.svg" alt="" />} 
               colorScheme='#4C847B;'  
               >
                <Link to="">Request new course</Link>
            </Button>
          </Box>
        </Box>
        <Box className="data-tables">
          <Table columns={columns} dataSource={data} 
                 pagination={{
                  pageSize: 9,
                  position: [bottom],
                }}
                size="small"
                loading={isPending}
                scroll={{
                 // y: 240,
                  x: 910.867
                }}
          />
        </Box>
      </Box> 
    </Box>
  )
}

export default Courses;
