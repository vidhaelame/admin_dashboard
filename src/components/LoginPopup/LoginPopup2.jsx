import { Formik, Field } from "formik";
import {
  Box,
  Button,
  Checkbox,
  Flex,
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
  VStack,
  InputGroup,
  InputLeftElement,
} from "@chakra-ui/react";
import { Link } from "react-router-dom";



export default function App() {
  return (
    <Flex style={{height: '286px'}}
    position="absolute"
    left="50%"
    top="50%"
    transform="translate(-50%, -40%);"
    padding={{ sm: "0 30px", md: "20px 20px 36px 20px", lg: "", xl:"" }}
    width={{ sm: "70%", md: "50%", lg: "45%", xl:"38%" }}
    //height= "286px"
    backgroundColor="#ffffff"
    boxShadow="0px 0px 32px rgba(9, 108, 159, 0.1)"
    borderRadius="16px"
   // bg="gray.100" 
    align="center" 
    justify="center" 
    h="100vh">
      <Box
       className="LoginPopup2"
       display="flex"
       flexDirection="column"
       alignItems="center"
       gap="25px"
      // bg="white" 
      // p={6} 
      // rounded="md" 
      // w={64}
       >
        <Formik
          initialValues={{
            email: "",
            password: "",
            rememberMe: false
          }}
          onSubmit={(values) => {
            alert(JSON.stringify(values, null, 2));
          }}
        >
          {({ handleSubmit, errors, touched }) => (
            <form onSubmit={handleSubmit}>
              <VStack spacing={4} align="flex-start">
                <FormControl>
                  <FormLabel htmlFor="email">Email Address</FormLabel>
                  <Field
                    as={Input}
                    id="email"
                    name="email"
                    type="email"
                    variant="filled"
                    //
                  />
                </FormControl>
                <FormControl isInvalid={!!errors.password && touched.password}>
                  <FormLabel htmlFor="password">Password</FormLabel>
                  <Field 
                    as={Input}
                    id="password"
                    name="password"
                    type="password"
                    variant="filled"
                    focusBorderColor="#4c847b"
                   // type="password"
                   width="300px"
                   backgroundColor="white"
                    borderColor="#DFDFDF"
                    placeholder="Password"
                    _placeholder={{ opacity: 1, color: "#252122", fontSize: "0.9rem" }}
                    validate={(value) => {
                      let error;

                      if (value.length < 6) {
                        error = "Password must contain at least 6 characters";
                      }

                      return error;
                    }}
                  />
                  <FormErrorMessage>{errors.password}</FormErrorMessage>
                </FormControl>
                <Box
                    display="flex"
                    flexDirection={{ sm: "column", md: "row", lg: "", xl:"" }}
                    alignItems="center"
                    justifyContent="space-between"
                    width="100%"
                    height="24px"
                    marginBottom="10px"
                    className="infos"
                > 
                   <Field color="#252122" colorScheme="green" borderColor="#252122"
                     as={Checkbox}
                     id="rememberMe"
                     name="rememberMe"
                   >
                      <Box fontSize="0.7rem" as="span">
                        Remember Password
                      </Box>
                   </Field>
                   <Box
                    as="p"
                    fontWeight="500"
                    fontSize="0.7rem"
                    lineHeight="120%"
                    color="#252122"
                  >
                  Forgot password?&nbsp;
                  <Box
                    as="a"
                    fontWeight="500"
                    fontSize="0.7rem"
                    lineHeight="120%"
                    color="#4c847b"
                    href="#"
                  >
                    Click Here
                 </Box>
                   </Box>
               </Box>
                {/* <Button type="submit" colorScheme="purple" width="full">
                  Login
                </Button> */}
                <Link style={{ width: "100%" }} className="btn-log" to="/home">
                   <Button
                     type="submit"
                     fontWeight="400"
                     fontSize="16px"
                     display="flex"
                     alignItems="center"
                     width="100%"
                     leftIcon={<img src="../src/assets/images/out.svg" alt="" />}
                     size="md"
                     colorScheme="linear-gradient(180deg, #69938F 0%, #4C847B 100%);"
                   >
                     Login
                   </Button>
                </Link>
              </VStack>
            </form>
          )}
        </Formik>
      </Box>
    </Flex>
  );
}