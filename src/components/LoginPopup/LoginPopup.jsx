import React from "react";
import {
  Box,
  Button,
  Checkbox,
  Input,
  InputGroup,
  InputLeftElement,
} from "@chakra-ui/react";
import { Link } from "react-router-dom";




const LoginPopup = () => {
  return (
    <Box
      className="LoginPopup"
      display="flex"
      flexDirection="column"
      alignItems="center"
      gap="25px"
      position="absolute"
      left="50%"
      top="50%"
      transform="translate(-50%, -40%);"
      padding={{ sm: "0 30px", md: "20px 50px 36px 50px", lg: "", xl:"" }}
      width={{ sm: "70%", md: "50%", lg: "45%", xl:"38%" }}
      height=" 286px"
      backgroundColor="#ffffff"
      boxShadow="0px 0px 32px rgba(9, 108, 159, 0.1)"
      borderRadius="16px"
    >
      <Box
        className="title"
        fontWeight="600"
        fontSize="1.5rem"
        line-height=" 120%"
        color=" #000000"
      >
        Login
      </Box>
      <Box
        display="flex"
        flexDirection="column"
        alignItems="flex-start"
        gap="10px"
        width="100%"
        className="form-content"
      >
        <InputGroup size="md">
          <InputLeftElement pointerEvents="none">
            <img src="../src/assets/images/user.svg" alt="" />
          </InputLeftElement>
          <Input
            focusBorderColor="#4c847b"
            type="text"
            borderColor="#DFDFDF"
            placeholder="Username"
            _placeholder={{ opacity: 1, color: "#252122", fontSize: "0.9rem" }}
          />
        </InputGroup>
        <InputGroup size="md">
          <InputLeftElement pointerEvents="none">
            <img width={15} src="../src/assets/images/lock.svg" alt="" />
          </InputLeftElement>
          <Input
            focusBorderColor="#4c847b"
            type="password"
            borderColor="#DFDFDF"
            placeholder="Password"
            _placeholder={{ opacity: 1, color: "#252122", fontSize: "0.9rem" }}
          />
        </InputGroup>
        <Box
          display="flex"
          flexDirection={{ sm: "column", md: "row", lg: "", xl:"" }}
          alignItems="center"
          justifyContent="space-between"
          width="100%"
          height="24px"
          marginBottom="10px"
          className="infos"
        >
          <Checkbox color="#252122" colorScheme="green" borderColor="#252122">
            <Box fontSize="0.7rem" as="span">
              Remember Password
            </Box>
          </Checkbox>
          <Box
            as="p"
            fontWeight="500"
            fontSize="0.7rem"
            lineHeight="120%"
            color="#252122"
          >
            Forgot password?&nbsp;
            <Box
              as="a"
              fontWeight="500"
              fontSize="0.7rem"
              lineHeight="120%"
              color="#4c847b"
              href="#"
            >
              Click Here
            </Box>
          </Box>
        </Box>
        <Link style={{ width: "100%" }} className="btn-log" to="/home">
          <Button
            fontWeight="400"
            fontSize="16px"
            display="flex"
            alignItems="center"
            width="100%"
            leftIcon={<img src="../src/assets/images/out.svg" alt="" />}
            size="md"
            colorScheme="linear-gradient(180deg, #69938F 0%, #4C847B 100%);"
          >
            Login
          </Button>
        </Link>
      </Box>
    </Box>
  );
};

export default LoginPopup;
