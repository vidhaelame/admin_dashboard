import React from "react";
import { Link } from "react-router-dom";
import { Box, Image } from "@chakra-ui/react";





const ItemMenuLeft = ({ name, link, ind, dataLenght, navLink, setOpen }) => {
  console.log(ind, dataLenght);
  return (
    <Box
      _hover={{
        backgroundColor: ind == 0 ? "rgba(255, 255, 255, 0.123)" : null,
        color: ind == 0 ? "#4c847b" : null,
      }}
      className="ItemMenuLeft"
    >
      <Link onClick={()=>
                setOpen(false) 
                } to={navLink}>
        <Box
          display="flex"
          justifyContent="start"
          alignItems="center;"
          padding="0 16px 10px 16px;"
          gap="10px;"
          width="100%;"
          height="40px;"
          marginBottom="5px;"
          className="item-dashboard"
        >
          <Box className="icon">
            <Image
              _hover={{
                cursor: "pointer;",
              }}
              boxSize="20px"
              width="100%"
              src={link}
              alt=""
            />
          </Box>
          <Box
            _hover={{
              color: ind == 0 ? "#4c847b" : null,
              cursor: "pointer;",
            }}
            fontSize="0.9rem"
            display="block"
            className="name text-white"
          >
            {name}
          </Box>
        </Box>
      </Link>
      <Box
        height="2px"
        backgroundColor="rgba(255, 255, 255, 0.2)"
        width="100%"
        marginBottom="10px"
        display={ind === dataLenght ? "none" : null}
        className={ind === dataLenght ? "border-line last" : "border-line"}
      ></Box>
    </Box>
  );
};

export default ItemMenuLeft;
